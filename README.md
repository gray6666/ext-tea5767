# ext-TEA5767

 **1 介绍** 


TEA5767 FM立体声收音机模块

1.本项目加载链接:[https://gitee.com/hmilycheng/ext-tea5767](https://gitee.com/hmilycheng/ext-tea5767)

2.author: DouDad

3.email: 1545148519@qq.com

4.license: MIT

5.id: TEA5767

6.version:0.0.2


 **2 积木列表（掌控板）** 


![输入图片说明](micropython/_images/%E7%A7%AF%E6%9C%A8.png)



 **3 示例程序（掌控板）** 



![输入图片说明](micropython/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F.png)



**4 积木列表（Arduino Uno）**



![输入图片说明](arduinoC/_images/%E7%A7%AF%E6%9C%A8.png)




 **5 示例程序（Arduino Uno）**



![输入图片说明](arduinoC/_images/%E7%A4%BA%E4%BE%8B%E7%A8%8B%E5%BA%8F.png)

