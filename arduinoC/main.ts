enum FREQUENCY{
    //%block="0.1"
    1,
    //%block="1.0"
    10,
    //%block="10.0"
    100
}


enum MUTE{
    //%block="静音"
    True,
    //%block="取消静音"
    False
}


enum MONO{
    //%block="单声道"
    True,
    //%block="立体声"
    False
}



enum DIRECTION{
    //%block="上"
    Up,
    //%block="下"
    Down
}


enum QUALITY{
    //%block="0"
    0,
    //%block="5"
    5,
    //%block="7"
    7,
    //%block="10"
    10
}





//% color="#0091ed" iconWidth=50 iconHeight=40
namespace TEA5765{
    //% block="TEA5767 Init MONO/STEREO:[MONO]" blockType="command"
    //% MONO.shadow="dropdownRound" MONO.options="MONO" MONO.defl="MONO.1"
    export function TEA5765Init(parameter: any, block: any) {
        let mono = parameter.MONO.code
        Generator.addInclude('Arduino','#include <Arduino.h>');
        Generator.addInclude('Wire','#include <Wire.h>');
        Generator.addInclude('radio','#include <radio.h>');
        Generator.addInclude('TEA5765','#include <TEA5767.h>'); 

        Generator.addObject(`radioObj`, `TEA5767`, `radio;`);
        Generator.addSetup(`radio.init`, `radio.init();`);
        Generator.addSetup(`radio.setMono`, `radio.setMono(${mono});`);
    }
   
	
	//% block="set Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="range" FREQUENCY.params.min=87.6 FREQUENCY.params.max=108 FREQUENCY.defl=91.6
    export function SetFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code;
        Generator.addCode(`radio.set_Frequency(${frequency});`);
    }



    //% block="Up Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="dropdown" FREQUENCY.options="FREQUENCY"
    export function UpFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code / 10;
        Generator.addCode(`radio.Change_Frequency(${frequency});`);
    }



    //% block="Down Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="dropdown" FREQUENCY.options="FREQUENCY"
    export function DownFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code / 10;
        Generator.addCode(`radio.Change_Frequency(-${frequency});`);
    }
    

    //% block="Seek Frequency[DIRECTION]" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTION"
    export function SeekFreq(parameter: any, block: any) {
	    let direction = parameter.DIRECTION.code;
        Generator.addCode(`radio.seek${direction}();`);
    }


    
    //% block="Get Frequency" blockType="reporter"
     export function GetFreq(parameter: any, block: any) {
        Generator.addCode(`radio.getFrequency();`);
    }


    //% block="Set Mute[MUTE] Mode" blockType="command"
    //% MUTE.shadow="dropdown" MUTE.options="MUTE"
    export function SetMute(parameter: any, block: any) {
        let mute =  parameter.MUTE.code; 
        Generator.addCode(`radio.setMute(${mute});`);
    }


    //% block="Set Mono[MONO] Mode" blockType="command"
    //% MONO.shadow="dropdown" MONO.options="MONO"
    export function SetMono(parameter: any, block: any) {
        let mono =  parameter.MONO.code; 
        Generator.addCode(`radio.setMono(${mono});`);
    }

}
