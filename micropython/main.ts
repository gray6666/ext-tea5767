enum FREQUENCY{
    //%block="0.1"
    1,
    //%block="1.0"
    10,
    //%block="10.0"
    100
}


enum MUTE{
    //%block="静音"
    True,
    //%block="取消静音"
    False
}


enum STANSBY{
    //%block="待机"
    True,
    //%block="取消待机"
    False
}



enum DIRECTION{
    //%block="向上"
    1,
    //%block="向下"
    0
}


enum QUALITY{
    //%block="0"
    0,
    //%block="5"
    5,
    //%block="7"
    7,
    //%block="10"
    10
}


enum LOOP{
    //%block="是"
    True,
    //%block="否"
    False
}


//% color="#0091ed" iconWidth=50 iconHeight=40
namespace TEA5765{
    //% block="set Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="range" FREQUENCY.params.min=87.6 FREQUENCY.params.max=108 FREQUENCY.defl=91.6
    export function SetFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from TEA5767 import Radio");
        //Generator.addCode(`i2c = I2C(scl=Pin(Pin.P19),sda=Pin(Pin.P20),freq=115200)`);
        Generator.addCode(`radio = Radio(i2c, freq=91.6)`);
        Generator.addCode(`radio.set_frequency(${frequency})`);
    }

    //% block="Increase Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="dropdown" FREQUENCY.options="FREQUENCY"
    export function UpFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code / 10;
        Generator.addCode(`radio.change_freqency(${frequency})`);
    }

    //% block="Reduce Frequency[FREQUENCY]" blockType="command"
    //% FREQUENCY.shadow="dropdown" FREQUENCY.options="FREQUENCY"
    export function DownFreq(parameter: any, block: any) {
        let frequency = parameter.FREQUENCY.code / 10;
        Generator.addCode(`radio.change_freqency(-${frequency})`);
    }
	

    //% block="Manual Search[DIRECTION][QUALITY]" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTION"
    //% QUALITY.shadow="dropdown" QUALITY.options="QUALITY" QUALITY.defl="QUALITY.7"
    export function ManualSearch(parameter: any, block: any) {
        let direction = parameter.DIRECTION.code;
		let quality = parameter.QUALITY.code; 
        Generator.addCode(`radio.manualsearch(${direction},${quality})`);
    }

	
    //% block="Get Frequency" blockType="reporter"
     export function GetFreq(parameter: any, block: any) {
        Generator.addCode(`round(radio.getfreq(),1)`);
    }


    //% block="Mute[Mute]" blockType="command"
    //% Mute.shadow="dropdown" Mute.options="Mute"
    export function Mute(parameter: any, block: any) {
        let mute =  parameter.Mute.code; 
        Generator.addCode(`radio.mute(${mute})`);
    }

    //% block="Standby[STANSBY]" blockType="command"
    //% STANSBY.shadow="dropdown" STANSBY.options="STANSBY"
    export function Standby(parameter: any, block: any) {
        let standby =  parameter.STANSBY.code;
        Generator.addCode(`radio.standby(${standby})`);
    }

    //% block="Mute[MUTE]" blockType="command"
    //% MUTE.shadow="dropdown" MUTE.options="MUTE" MUTE.defl="MUTE.1"
    export function Mute(parameter: any, block: any) {
        let mute =  parameter.MUTE.code; 
        Generator.addCode(`radio.mute(${mute})`);
    }

    //% block="Standby[STANSBY]" blockType="command"
    //% STANSBY.shadow="dropdown" STANSBY.options="STANSBY" STANSBY.defl="STANSBY.1"
    export function Standby(parameter: any, block: any) {
        let stansby =  parameter.STANSBY.code;
        Generator.addCode(`radio.standby(${stansby})`);
    }

}
